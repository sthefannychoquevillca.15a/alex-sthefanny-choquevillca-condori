/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenlistas;

public class nodo<T> {
    private T value;
    private nodo<T> next;
    
    public nodo(T v, nodo<T> n){
        this.value = v;
        this.next = n;
    }

    public T getValue() { 
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public nodo<T> getNext() {
        return next;
    }

    public void setNext(nodo<T> next) {
        this.next = next;
    }
    
}
