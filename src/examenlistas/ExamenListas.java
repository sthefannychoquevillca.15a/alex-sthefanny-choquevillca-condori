/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenlistas;

import java.util.Iterator;

/**
 *
 * @author Mix
 */
public class ExamenListas {

 public class listadinamica<T> implements Iterable<T> {
    private nodo<T> first;
    private nodo<T> last;
    private int size;
    
    public listadinamica(){
        this.first = null;
        this.last = null;
        this.size = 0;
    }
    
    public boolean A(){
        return size==0;
    }
    
    public int sizeList(){
        return size;
    }
    
    private nodo<T> getNode(int index){
        if(A() || (index<0 || index >=sizeList())){
            return null;
        }else if(index == 0){
            return first;
        }else if(index == sizeList() -1){
            return last;
        }else{
            nodo<T> searchNodo = first;
            int count = 0;
            while(count < index){
                count++;
                searchNodo = searchNodo.getNext();
            }
            return searchNodo;
        }
    }
    
    public T get(int index){
        if(A() || (index<0 || index >=sizeList())){
            return null;
        }else if(index == 0){
            return first.getValue();
        }else if(index == sizeList() -1){
            return last.getValue();
        }else{
            nodo<T> searchNodeValue = getNode(index);
            return searchNodeValue.getValue();
        }
    }
    
    public T getFirst(){
        if(A()){
            return null;
        }else{
            return first.getValue();
        }
    }
    
    public T getLast(){
        if(A()){
            return null;
        }else{
            return last.getValue();
        }
    }
    
    
    public T addFirst(T value){
        nodo<T> newvalue;
        if(A()){
            newvalue = new nodo<>(value,null);
            first = newvalue;
            last = newvalue;
        }else{
            newvalue = new nodo<>(value,first);
            first = newvalue;
        }
        size++;
        return first.getValue();
    }
    
    public T addLast(T value){
        nodo<T> newvalue;
        if(A()){
            return addFirst(value);
        }else{
            newvalue = new nodo<>(value,null);
            last.setNext(newvalue);
            last = newvalue;
        }
        size++;
        return last.getValue();
    }
    
    public T add(T value,int index){
        if(index == 0){
            return addFirst(value);
        }else if(index == sizeList()){
            return addLast(value);
        }else if((index < 0 || index >= sizeList()-1)){
            return null;
        }else{
            nodo<T> nodo_prev = getNode(index-1);
            nodo<T> nodo_current = getNode(index);
            nodo<T> newvalue = new nodo<>(value,nodo_current);
            nodo_prev.setNext(newvalue);
            size++;
            return getNode(index).getValue();
        }
    }
    
    public T removeFirst(){
        if(A()){
            return null;
        }else{
            T value = first.getValue(); //[1] -> [2] -> [3]
            nodo<T> aux = first.getNext();
            first = aux;
            if(sizeList() == 1){
                last = null;
            }
            size--;
            return value;
        }
    }
    
    public T removeLast(){
        if(A()){
            return null;
        }else{
            T value = last.getValue();
            nodo<T> newLast = getNode(sizeList()-2);
            if(newLast == null){
                last = null;
                if(sizeList() == 2){
                    last = first;
                }else{
                    first = null;
                }
            }else{
                last =  newLast;
                last.setNext(null);
            }
            size--;
            return value;
        }
    }
    
    public T remove(int index){//[1]->[2]->[3]->[4]
        if(index == 0){
            return removeFirst();
        }else if(index == sizeList()){
            return removeLast();
        }else if(A() || (index < 0 || index >= sizeList())){
            return null;
        }else{
            nodo<T> nodo_current = getNode(index);//[3]
            nodo<T> nodo_prev = getNode(index - 1);//[2]
            nodo<T> nodo_current_next = nodo_current.getNext();//[4]
            T value = nodo_current.getValue();//3
            nodo_current = null;
            nodo_prev.setNext(nodo_current_next);
            size--;
            return value;
        }
    }
    
    public T modifyValuesOfNode(T newvalue, int index){
        if(A() || (index < 0 || index >= sizeList())){
            return null;
        }else{//[1] -> [2] -> [3]
            nodo<T> current_node = getNode(index);
            current_node.setValue(newvalue);
            return current_node.getValue();
        }
    }
    
    public Integer indexOfValue(T value){
        if(A()){
            return null;
        }else{
            nodo<T> element = first;
            int index = 0;
            while(element != null){
                if(value == element.getValue()){
                    return index;
                }
                index++;
                element = element.getNext();
            }
        }
        return null;
    }
    
    public String contentValuesList(){
        String str = "";
        if(A()){
            str = "Lista Vacia";
        }else{
            nodo<T> out = first;
            while(out != null){
                str = str + out.getValue()+ " - ";
                out = out.getNext();
            }
        }
        return str;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyIterator();
    }
    
    
    private class MyIterator<listadinamica> implements Iterator{
        private int index;
        @Override
        public boolean hasNext() {//
            return getNode(index) != null; 
        }
        @Override
        public Object next() {
           T value = getNode(index).getValue();
           index++;
           return value;
        }
    }

  }
}

